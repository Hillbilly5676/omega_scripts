local m=120238004
local cm=_G["c"..m]
cm.name="合成魔兽 加泽特"
function cm.initial_effect(c)
	RD.CreateAdvanceSummonFlag(c,20238004)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(cm.atkval)
	c:RegisterEffect(e1)
	--Material Check
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_MATERIAL_CHECK)
	e2:SetLabelObject(e1)
	e2:SetValue(cm.check)
	c:RegisterEffect(e2)
	--Continuous Effect
	RD.AddContinuousEffect(c,e1)
end
--Material Check
function cm.check(e,c)
	local atk=0
	local g=c:GetMaterial()
	if g:GetCount()==1 then
		atk=RD.GetBaseAttackOnTribute(g:GetFirst())*2
	elseif g:GetCount()==2 then
		atk=RD.GetBaseAttackOnTribute(g:GetFirst())+RD.GetBaseAttackOnTribute(g:GetNext())
	end
	if atk>0 then
		e:GetLabelObject():SetLabel(atk)
	else
		e:GetLabelObject():SetLabel(0)
	end
end
--Atk Up
function cm.atkval(e,c)
	if c:GetFlagEffect(20238004)~=0 then return e:GetLabel() else return 0 end
end