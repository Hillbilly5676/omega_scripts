local m=120253057
local list={120125001,120145000,120238029}
local cm=_G["c"..m]
cm.name="流星炎融合"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Activate
	local e1=RD.CreateFusionEffect(c,nil,nil,nil,0,0,cm.matcheck,RD.FusionToGrave,nil,cm.operation)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_FUSION_SUMMON+CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
end
--Activate
function cm.matfilter(c)
	return c:IsFusionCode(list[1],list[2],list[3])
end
function cm.matcheck(tp,sg,fc)
	return sg:IsExists(cm.matfilter,1,nil) and sg:GetCount()==2
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp,mat,fc)
	if not fc:IsType(TYPE_EFFECT) then
		RD.CanSelectAndDoAction(aux.Stringid(m,1),HINTMSG_DESTROY,nil,tp,0,LOCATION_ONFIELD,1,1,nil,function(g)
			Duel.Destroy(g,REASON_EFFECT)
		end)
	end
end