-- Rush Duel 规则
RushDuel = RushDuel or {}

-- 初始化
function RushDuel.Init()
    RushDuel.InitRule()
    RushDuel.InitFlag()
    Duel.BreakEffect = function()
        -- "那之后" 不打断时点
    end
    -- 决斗开始
    RushDuel.CreateFieldGlobalEffect(true, EVENT_PHASE_START + PHASE_DRAW, function(e)
        -- 传说卡
        RushDuel.InitLegend()
        -- 先攻抽卡
        if not Auxiliary.Load2PickRule then
            Duel.Draw(0, 1, REASON_RULE)
        end
        e:Reset()
    end)
end
-- 初始化规则
function RushDuel.InitRule()
    -- 禁用最左与最右列
    RushDuel.CreateFieldGlobalEffect(false, EFFECT_DISABLE_FIELD, function(e, tp)
        return 0x11711171
    end)
    -- 抽卡阶段, 抽卡至5张, 超过5张时改为抽1张
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_DRAW_COUNT, function(e)
        return math.max(1, 5 - Duel.GetFieldGroupCount(Duel.GetTurnPlayer(), LOCATION_HAND, 0))
    end)
    -- 跳过准备阶段
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_SKIP_SP)
    -- 召唤次数无限制
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_SET_SUMMON_COUNT_LIMIT, 100)
    -- 场上的怪兽的效果强制1回合1次
    local function get_effect_owner_code(e)
        if e:GetType() & EFFECT_TYPE_XMATERIAL == EFFECT_TYPE_XMATERIAL then
            -- 极大怪兽的L/R部分的效果分开计算
            return e:GetLabel()
        else
            return e:GetOwner():GetOriginalCode()
        end
    end
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_CANNOT_ACTIVATE, function(e, re, tp)
        return re:GetHandler():GetFlagEffect(get_effect_owner_code(re)) ~= 0
    end)
    RushDuel.CreateFieldGlobalEffect(true, EVENT_CHAIN_SOLVING, function(e, tp, eg, ep, ev, re, r, rp)
        local te = Duel.GetChainInfo(ev, CHAININFO_TRIGGERING_EFFECT)
        local code = get_effect_owner_code(te)
        te:GetHandler():RegisterFlagEffect(code, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
    end)
    -- 同一时点只能发动一张陷阱卡
    local function is_trap(e)
        return e:IsHasType(EFFECT_TYPE_ACTIVATE) and e:IsActiveType(TYPE_TRAP)
    end
    local function tarp_limit(e, rp, tp)
        return not is_trap(e)
    end
    RushDuel.CreateFieldGlobalEffect(true, EVENT_CHAINING, function(e, tp, eg, ep, ev, re, r, rp)
        if is_trap(re) then
            Duel.SetChainLimit(tarp_limit)
        end
    end)
    -- 跳过主要阶段2
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_SKIP_M2)
    -- 手卡无限制
    RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_HAND_LIMIT, 100)
    -- 极大怪兽
    RushDuel.InitMaximum()
end
-- 初始化标记
function RushDuel.InitFlag()
    local reg_summon = function(e, tp, eg, ep, ev, re, r, rp)
        eg:ForEach(function(tc)
            tc:RegisterFlagEffect(FLAG_SUMMON_TURN, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
        end)
    end
    local reg_attack = function(e, tp, eg, ep, ev, re, r, rp)
        Duel.RegisterFlagEffect(tp, FLAG_ATTACK_ANNOUNCED, RESET_PHASE + PHASE_DAMAGE, 0, 1)
    end
    RushDuel.CreateFieldGlobalEffect(true, EVENT_SUMMON_SUCCESS, reg_summon)
    RushDuel.CreateFieldGlobalEffect(true, EVENT_SPSUMMON_SUCCESS, reg_summon)
    RushDuel.CreateFieldGlobalEffect(true, EVENT_ATTACK_ANNOUNCE, reg_attack)
end