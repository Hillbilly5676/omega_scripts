local m=120257008
local cm=_G["c"..m]
cm.name="穿越侍·高天海牛侍 铠天原新星"
function cm.initial_effect(c)
	--Summon Procedure
	RD.AddSummonProcedureOne(c,aux.Stringid(m,0),nil,nil,cm.sumop1)
	RD.AddSummonProcedureThree(c,aux.Stringid(m,1))
	RD.CreateAdvanceSummonFlag(c,20257008)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(cm.atkval)
	c:RegisterEffect(e1)
	--Material Check
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_MATERIAL_CHECK)
	e2:SetLabelObject(e1)
	e2:SetValue(cm.check)
	c:RegisterEffect(e2)
	--Continuous Effect
	RD.AddContinuousEffect(c,e1)
end
--Summon Procedure
function cm.sumop1(e,tp,eg,ep,ev,re,r,rp,c,mg)
	--Change Base Attack
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_BASE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(1800)
	e1:SetReset(RESET_EVENT+0xff0000)
	c:RegisterEffect(e1)
end
--Material Check
function cm.check(e,c)
	if not c:IsSummonType(SUMMON_VALUE_THREE) then return end
	local mg=c:GetMaterial()
	if mg:GetCount()==3 then
		lv=mg:GetSum(Card.GetOriginalLevel)
	elseif mg:GetCount()==2 then
		local ng,dg=RD.SplitTribute(c)
		if dg:GetCount()==1 then
			lv=ng:GetFirst():GetOriginalLevel()+dg:GetFirst():GetOriginalLevel()*2
		else
			local _,max=mg:GetMaxGroup(Card.GetOriginalLevel)
			lv=mg:GetSum(Card.GetOriginalLevel)+max
		end
	elseif mg:GetCount()==1 then
		lv=mg:GetFirst():GetOriginalLevel()*3
	end
	if lv>0 then
		e:GetLabelObject():SetLabel(lv*100)
	else
		e:GetLabelObject():SetLabel(0)
	end
end
--Atk Up
function cm.atkval(e,c)
	if c:GetFlagEffect(20257008)~=0 and c:IsSummonType(SUMMON_VALUE_THREE) then return e:GetLabel() else return 0 end
end